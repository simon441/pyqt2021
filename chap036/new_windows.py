from typing import Union
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QMdiArea, QMdiSubWindow, QPushButton, QTextEdit, QWidget
from PyQt5 import uic
import sys
from pathlib import Path


class Ui(QMainWindow):
    # Number of windows
    count = 0

    def __init__(self) -> None:
        super(Ui, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'new.ui'), self)

        # Define the Widgets
        self.mdi: QMdiArea = self.findChild(
            QMdiArea, 'mdiArea')
        self.label: QLabel = self.findChild(QLabel, 'label')
        self.button: QPushButton = self.findChild(
            QPushButton, 'pushButton')

        # Actions Click Button
        self.button.clicked.connect(self.add_window)

        # Show the App
        self.show()

    def add_window(self):
        """Add a new window to the MDI"""
        Ui.count = Ui.count + 1
        # Create a Sub Window
        sub = QMdiSubWindow()
        # Set widgets in the sub window
        sub.setWidget(QTextEdit())
        # Set the Titlebar of the sub window
        sub.setWindowTitle('Subby Window' + str(Ui.count))
        # Add the sub window into the MDI Widget
        self.mdi.addSubWindow(sub)

        # Show the new sub window
        sub.show()

        # Position the sub windows
        # Tile the windows
        self.mdi.tileSubWindows()

        # Cascade windows
        self.mdi.cascadeSubWindows()

        # self.mdi.closeActiveSubWindow()
        # self.mdi.removeSubWindow()
        print(self.mdi.subWindowList())
        # self.mdi.closeAllSubWindows()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    UIWindow = Ui()
    sys.exit(app.exec_())
