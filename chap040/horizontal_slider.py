from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QSlider
from PyQt5 import QtCore, uic
import sys
from pathlib import Path


class Ui(QMainWindow):
    def __init__(self) -> None:
        super().__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'slide.ui'), self)
        self.setWindowTitle('Slider')

        # Define the Widgets
        slider: QSlider = self.findChild(QSlider, 'horizontalSlider')
        self.label: QLabel = self.findChild(QLabel, 'label')

        # Set Slider Properties
        slider.setMinimum(0)
        slider.setMaximum(50)
        slider.setValue(0)
        slider.setTickPosition(QSlider.TickPosition.TicksBothSides)
        slider.setTickInterval(5)
        slider.setSingleStep(5)

        # Set the label text to the slider position
        self.label.setText(str(slider.value()))
        # Center the label
        self.label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        # Move the Slider
        slider.valueChanged.connect(self.onSlide)

        # Show the App Window
        self.show()

    def onSlide(self, value: int):
        self.label.setText(str(value))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = Ui()
    sys.exit(app.exec())
