from functools import reduce
import random
import sys
from pathlib import Path
from typing import List, OrderedDict, Union

from PyQt5 import uic
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QMessageBox, QPushButton, QStyle


class Ui(QMainWindow):
    DEALER_NAME = 'dealer'
    PLAYER_NAME = 'player'

    def __init__(self, maxCardsPerPlayer=5) -> None:
        """[summary]

        Args:
            maxCardsPerPlayer (int, optional): [Maximum cards per player]. Defaults to 5.
        """
        super(Ui, self).__init__()

        self.maxCardsPerPlayer = maxCardsPerPlayer

        # Define variables
        # Define the initial Deck
        self.deck: List[str] = []
        # Player's decks
        self.dealer: List[str] = []
        self.player: List[str] = []
        # Scores
        self.dealerScore: List[int] = []
        self.playerScore: List[int] = []
        # Blackjack status for each player (True=win, False=lose, None=bust)
        self.blackjackStatus: dict[str, Union[bool, None]] = {self.DEALER_NAME: False, self.PLAYER_NAME: False}
        # Keep track of score details
        self.dealerTotal = 0
        self.playerTotal = 0
        self.total = {self.DEALER_NAME: 0, self.PLAYER_NAME: 0}

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'blackjack.ui'), self)
        self.setWindowTitle('Blackjack')

        # Define widgets
        self.dealerHeaderLabel: QLabel = self.findChild(
            QLabel, 'dealerHeaderLabel')
        self.playerHeaderLabel: QLabel = self.findChild(
            QLabel, 'playerHeaderLabel')

        self.dealerCard1: QLabel = self.findChild(QLabel, 'dealerCardLabel_1')
        self.dealerCard2: QLabel = self.findChild(QLabel, 'dealerCardLabel_2')
        self.dealerCard3: QLabel = self.findChild(QLabel, 'dealerCardLabel_3')
        self.dealerCard4: QLabel = self.findChild(QLabel, 'dealerCardLabel_4')
        self.dealerCard5: QLabel = self.findChild(QLabel, 'dealerCardLabel_5')

        self.playerCard1: QLabel = self.findChild(QLabel, 'playerCardLabel_1')
        self.playerCard2: QLabel = self.findChild(QLabel, 'playerCardLabel_2')
        self.playerCard3: QLabel = self.findChild(QLabel, 'playerCardLabel_3')
        self.playerCard4: QLabel = self.findChild(QLabel, 'playerCardLabel_4')
        self.playerCard5: QLabel = self.findChild(QLabel, 'playerCardLabel_5')

        self.shufflePushButton: QPushButton = self.findChild(
            QPushButton, 'shufflePushButton')
        self.hitMeButton: QPushButton = self.findChild(
            QPushButton, 'hitPushButton')
        self.standPushButton: QPushButton = self.findChild(
            QPushButton, 'standPushButton')

        # Click buttons
        self.shufflePushButton.clicked.connect(self.shuffleCards)
        self.hitMeButton.clicked.connect(self.playerHit)
        self.standPushButton.clicked.connect(self.stand)

        # Shuffle the Cards
        self.shuffleCards()

        # Show the App
        self.show()
        
    
    def stand(self):
        """Stand"""
        # Disable buttons
        self._disableButtons()
        # Reset total scores
        self.dealerTotal = 0
        self.playerTotal = 0
        self.total: dict[str, int] = {self.DEALER_NAME: 0, self.PLAYER_NAME: 0}

        # Get the Player Score
        self.playerTotal = reduce(lambda a, b: a+b, self.playerScore, 0)
        
        # Get the Dealer Score
        self.dealerTotal = reduce(lambda a, b: a+b, self.dealerScore, 0)
        
        # Logic
        if self.dealerTotal>=17:
            # Check for Bust
            if self.dealerTotal>21:
                # Bust
                self._showBlackjackMessage('Player Wins!', f'Player Wins!\nDealer has: {self.dealerTotal} Player has: {self.playerTotal}')
            elif self.dealerTotal==self.playerTotal:
                # Tie
                self._showBlackjackMessage('Tie!', f'Push! It\'s a Tie!\nDealer has: {self.dealerTotal} Player has: {self.playerTotal}')
            elif self.dealerTotal>self.playerTotal:
                # Dealer wins
                self._showBlackjackMessage('Dealer  Wins!', f'Dealer Wins!\nDealer has: {self.dealerTotal} Player has: {self.playerTotal}')
            else:
                # Player wins
                self._showBlackjackMessage('Player Wins!', f'Player Wins!\nDealer has: {self.dealerTotal} Player has: {self.playerTotal}')
            
        else:
            # Dealer needs another card!
            self.dealerHit()
            self.stand()
        

    def shuffleCards(self):
        """Shuffle the cards
        """
        # Reset total scores
        self.dealerTotal = 0
        self.playerTotal = 0
        self.total: dict[str, int] = {self.DEALER_NAME: 0, self.PLAYER_NAME: 0}

        # Reset dictionary keeping track of the blackjack status of each player
        self.blackjackStatus = {self.DEALER_NAME: False, self.PLAYER_NAME: False}

        # Enable buttons
        self.hitMeButton.setEnabled(True)
        self.standPushButton.setEnabled(True)

        # Reset card images
        pixmap = QPixmap(str(Path.joinpath(
            Path(__file__).parent.parent.resolve(), 'images', 'green.png')))
        self.dealerCard1.setPixmap(pixmap)
        self.dealerCard2.setPixmap(pixmap)
        self.dealerCard3.setPixmap(pixmap)
        self.dealerCard4.setPixmap(pixmap)
        self.dealerCard5.setPixmap(pixmap)

        self.playerCard1.setPixmap(pixmap)
        self.playerCard2.setPixmap(pixmap)
        self.playerCard3.setPixmap(pixmap)
        self.playerCard4.setPixmap(pixmap)
        self.playerCard5.setPixmap(pixmap)
        # Define the Deck
        suits = ['clubs', 'diamonds', 'hearts', 'spades']
        # 11 => Jack, 12 => Queen, 13 => King, 14 => Ace
        values = range(2, 15)

        # Create a Deck
        self.deck.clear()

        for suit in suits:
            for value in values:
                self.deck.append(f'{value}_of_{suit}')

        # Create the Players
        self.dealer.clear()
        self.player.clear()
        self.dealerScore.clear()
        self.playerScore.clear()
        self.dealerSpot = 0
        self.playerSpot = 0

        self.dealerHit()
        self.dealerHit()

        self.playerHit()
        self.playerHit()

    def dealCards(self):
        """Deal the Cards"""
        try:
            # Grab a random card for the dealer
            card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(card)
            # Add that card to dealer's list
            self.dealer.append(card)
            # Output current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
            pixmap = QPixmap(f'{path}')
            self.dealerCard1.setPixmap(pixmap)

            # Grab a random card for the player
            card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(card)
            # Add that card to player's list
            self.player.append(card)
            # Ouput current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
            pixmap = QPixmap(f'{path}')
            self.playerCard1.setPixmap(pixmap)

            if (len(self.deck) <= 0):
                raise IndexError()

            # Update Title bar
            self.setWindowTitle(
                f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
        except IndexError as e:
            self.setWindowTitle('Game Over')

    def dealerHit(self):
        """Deal the Cards for the Dealer"""
        if self.dealerSpot <= self.maxCardsPerPlayer:
            try:
                # Grab a random card for the dealer
                card = random.choice(self.deck)
                # Remove that card from the Deck
                self.deck.remove(card)
                # Add that card to dealer's list
                self.dealer.append(card)

                # Add Card to Dealers' score
                self.dealerScore.append(self._getCardValue(card, self.dealerSpot))
                # Ouput current card to screen
                path = Path.joinpath(
                    Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
                pixmap = QPixmap(f'{path}')

                if self.dealerSpot == 0:
                    self.dealerCard1.setPixmap(pixmap)

                elif self.dealerSpot == 1:
                    self.dealerCard2.setPixmap(pixmap)

                elif self.dealerSpot == 2:
                    self.dealerCard3.setPixmap(pixmap)

                elif self.dealerSpot == 3:
                    self.dealerCard4.setPixmap(pixmap)

                elif self.dealerSpot == 4:
                    self.dealerCard5.setPixmap(pixmap)
                    
                    # Check for bust
                    # Grab total scores
                    # self.playerTotal = reduce(lambda a, b: a+b, self.playerScore, 0)
                    # self.dealerScore = reduce(lambda a, b: a+b, self.dealerScore, 0)

                    # Get dealer score
                    self.dealerTotal = 0
                    for score in self.dealerScore:
                        self.dealerTotal += score
                        
                    # Get player score
                    self.playerTotal = 0
                    for score in self.playerScore:
                        self.playerTotal += score
                        
                    # Check to see if <= 21
                    if self.dealerTotal <=21:
                        # Win!
                        # Disable all buttons
                        self._disableButtons()
                        
                        # Flash up a win message
                        self._showBlackjackMessage('Dealer Wins!', f'Dealer Wins: {self.playerTotal}   Dealer: {self.dealerTotal}')


                self.dealerSpot += 1

                if (len(self.deck) <= 0):
                    raise IndexError()

                # Update Title bar
                self.setWindowTitle(
                    f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
            except IndexError as e:
                self.setWindowTitle('Game Over')

            # Check for BlackJack
            self.blackjackCheck(self.DEALER_NAME)

    def playerHit(self):
        """Deal the Cards for the Player"""
        if self.playerSpot <= self.maxCardsPerPlayer:
            try:
                # Grab a random card for the player
                card = random.choice(self.deck)
                # Remove that card from the Deck
                self.deck.remove(card)
                # Add that card to player's list
                self.player.append(card)

                # Add Card to Players' score
                self.playerScore.append(self._getCardValue(card, self.playerSpot))

                # Ouput current card to screen
                path = Path.joinpath(
                    Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
                pixmap = QPixmap(f'{path}')

                if self.playerSpot == 0:
                    self.playerCard1.setPixmap(pixmap)

                elif self.playerSpot == 1:
                    self.playerCard2.setPixmap(pixmap)

                elif self.playerSpot == 2:
                    self.playerCard3.setPixmap(pixmap)

                elif self.playerSpot == 3:
                    self.playerCard4.setPixmap(pixmap)

                elif self.playerSpot == 4:
                    self.playerCard5.setPixmap(pixmap)

                    # Check for bust
                    # Grab total scores
                    self.playerTotal = reduce(lambda a, b: a+b, self.playerScore, 0)
                    self.dealerTotal = reduce(lambda a, b: a+b, self.dealerScore, 0)

                    # Get dealer score
                    self.dealerTotal = 0
                    for score in self.dealerScore:
                        self.dealerTotal += score
                        
                    # Get player score
                    self.playerTotal = 0
                    for score in self.playerScore:
                        self.playerTotal += score
                        
                    # Check to see if <= 21
                    if self.playerTotal <=21:
                        # Win!
                        # Disable all buttons
                        self._disableButtons()
                        
                        # Flash up a win message
                        self._showBlackjackMessage('Player Wins!', f'Player Wins: {self.playerTotal}   Dealer: {self.dealerTotal}')




                self.playerSpot += 1

                if (len(self.deck) <= 0):
                    raise IndexError()

                # Update Title bar
                self.setWindowTitle(
                    f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
            except IndexError as e:
                self.setWindowTitle('Game Over')

            # Check for BlackJack
            self.blackjackCheck(self.PLAYER_NAME)

    def blackjackCheck(self, playerName: str) -> None:
        """Check for Blackjack (score is 21)

        Args:
            playerName (str): [Player name]
        """
        # Reset total scores
        self.dealerTotal = 0
        self.playerTotal = 0
        self.total: dict[str, int] = {self.DEALER_NAME: 0, self.PLAYER_NAME: 0}

        if playerName in [self.PLAYER_NAME, self.DEALER_NAME]:
            if playerName == self.DEALER_NAME:
                self._blackJackMessage(self.dealerScore, self.DEALER_NAME)
            if playerName == self.PLAYER_NAME:
                self._blackJackMessage(self.playerScore, self.PLAYER_NAME)
            # Check for blackjack
            if len(self.playerScore) == 2 and len(self.dealerScore) == 2:
                # Check for tie
                if self.blackjackStatus[self.DEALER_NAME] == True and self.blackjackStatus[self.PLAYER_NAME] == True:
                    self._showBlackjackMessage('Push!!', "It's a Tie!!")
                # Dealer wins
                elif self.blackjackStatus[self.DEALER_NAME] == True:
                    self._showBlackjackMessage(f'Dealer Wins!', f'Blackjack!\nDealer Wins!')
                # Player wins
                elif self.blackjackStatus[self.PLAYER_NAME] == True:
                    self._showBlackjackMessage(f'Player Wins!', f'Blackjack!\nPlayer Wins!')
            else:
               # Check for tie
                if self.blackjackStatus[self.DEALER_NAME] == True and self.blackjackStatus[self.PLAYER_NAME] == True:
                    self._showBlackjackMessage('Push!!', "It's a Tie!!")
                # Dealer wins
                elif self.blackjackStatus[self.DEALER_NAME] == True:
                    self._showBlackjackMessage(f'21! Dealer Wins!', f'Blackjack!\nDealer Wins!')
                # Player wins
                elif self.blackjackStatus[self.PLAYER_NAME] == True:
                    self._showBlackjackMessage(f'21! Player Wins!', f'Blackjack!\nPlayer Wins!')

            # Check for player bust
            if playerName == self.PLAYER_NAME and self.blackjackStatus[self.PLAYER_NAME] == None:
                # Bust message
                self._showBlackjackMessage(f'Bust!!', f'Player Loses! {self.playerTotal}')

    def _getCardValue(self, cardName: str, spot: int = 0) -> int:
        """Get card score

        Args:
            cardName (str): card
            sport (int): Dealer or Player current spot
        Returns:
            int: card value
        """
        card = int(cardName.split('_', 1)[0])
        if card == 14:  # Ace
            return 11 if spot <= 2 else 1
        elif card in (11, 12, 13):
            return 10
        return card

    def _blackJackMessage(self, score: List[int], playerName: str) -> None:
        """If blackjack, show win message

        Args:
            score (List[int]): current player scores
            playerName (str)
        """

        if len(score) == 2:  # two cards (start of game
            if score[0] + score[1] == 21:
                # Change blackjack status to Won (True)
                self.blackjackStatus[playerName] = True

                # Disable buttons
                self._disableButtons()
        elif playerName == self.PLAYER_NAME:
            # Loop through player score add add up cards
            for s in score:
                # Add up the score
                self.playerTotal += s
                # Check for win or lose
                # Win
                if self.playerTotal == 21:
                    self.blackjackStatus[playerName] = True
                # Bust
                elif self.playerTotal > 21:
                    # Check for ace conversion
                    self._makeAceConversion()
                    # Update player status: win/lose
                    self._updatePlayerBlackjackStatus(self.PLAYER_NAME)

                    # for cardNumber, cardValue in enumerate(self.playerScore):
                    #     if cardValue == 11:
                    #         # Change 11 to 1
                    #         self.playerScore[cardNumber] = 1

                    #         # Update player's total
                    #         self.playerTotal = 0
                    #         # Add up the score
                    #         self.playerTotal = reduce(lambda a, b: a+b, self.playerScore, 0)

                    #         # Check for win/bust
                    #         if self.playerTotal > 21:
                    #             # Bust
                    #             self.blackjackStatus[self.PLAYER_NAME] = None
                    # else:
                    #     self._updatePlayerBlackjackStatus(self.PLAYER_NAME)
                        

                    # self.blackjackStatus[playerName] = None
            if self.blackjackStatus[playerName] == None:
                # Disable buttons
                self._disableButtons()

    def _showBlackjackMessage(self, title: str, text: str) -> None:
        """Show the blackjack message in a QMessageBox

        Args:
            title (str): message title
            text (str): message body
        """
        message=QMessageBox(self)
        message.setStyleSheet('.QMessageBox{color: #fff;font-size:20px;}QLabel,QPushButton{color: #fff;font-size:20px;}')
        message.setWindowTitle(title)
        message.setText(text)
        message.show()

    def _updatePlayerBlackjackStatus(self, playerName: str):
        """_updatePlayerBlackjackStatus Player win, bust, lose?
        """
        if self.playerTotal == 21:
            self.blackjackStatus[playerName]=True
        # Bust
        elif self.playerTotal > 21:
            self.blackjackStatus[playerName]=None
        else:
            self.blackjackStatus[playerName]=False

    def _makeAceConversion(self):
        """_makeAceConversion Convert Ace as value 11 to value 1\
            then recalculate player score
        """ 
        for cardNumber, cardValue in enumerate(self.playerScore):
            if cardValue == 11:
                # Change 11 to 1
                self.playerScore[cardNumber] = 1

                # Update player's total
                self.playerTotal = 0
                # Add up the score
                self.playerTotal = reduce(lambda a, b: a+b, self.playerScore, 0)

                # Check for win/bust
                if self.playerTotal > 21:
                    # Bust
                    self.blackjackStatus[self.PLAYER_NAME] = None
                    return
                
        
    def _disableButtons(self) -> None:
        """Disable all Buttons"""
        # Disable buttons
        self.hitMeButton.setEnabled(False)
        self.standPushButton.setEnabled(False)


# Initialize the Application
if __name__ == '__main__':
    app=QApplication(sys.argv)
    UiWindow=Ui()
    sys.exit(app.exec())
