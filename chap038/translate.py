import sys
from pathlib import Path
from typing import Union

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QComboBox, QMainWindow, QMessageBox, QPushButton, QTextEdit
import googletrans
import textblob


class Ui(QMainWindow):
    def __init__(self) -> None:
        super(Ui, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'translate.ui'), self)
        self.setWindowTitle('Translator App')

        # Define the Widgets
        self.translateButton: QPushButton = self.findChild(
            QPushButton, 'translateButton')
        self.clearButton: QPushButton = self.findChild(
            QPushButton, 'clearButton')

        self.comboFrom: QComboBox = self.findChild(QComboBox, 'comboBoxFrom')
        self.comboTo: QComboBox = self.findChild(QComboBox, 'comboBoxTo')

        self.textEditFrom: QTextEdit = self.findChild(
            QTextEdit, 'textEditFrom')
        self.textEditTo: QTextEdit = self.findChild(QTextEdit, 'textEditTo')

        # Click the buttons
        self.translateButton.clicked.connect(self.translate)
        self.clearButton.clicked.connect(self.clear)

        # Add languages to the combo boxes
        self.languages = googletrans.LANGUAGES
        self.language_list = list(self.languages.values())

        # Add items to combo boxes
        self.comboFrom.addItems(self.language_list)
        self.comboTo.addItems(self.language_list)

        # Set default combo item
        self.comboFrom.setCurrentText("english")
        self.comboTo.setCurrentText("french")

        # Show the App
        self.show()

    def translate(self):
        """Translate entered text"""
        try:
            # Get original language key
            from_language_key = self.get_language_key(
                self.comboFrom.currentText())
            # Get translated language key
            to_language_key = self.get_language_key(self.comboTo.currentText())

            # Turn originaltext into a textblob
            words = textblob.TextBlob(self.textEditFrom.toPlainText())

            # Translate words
            words = words.translate(
                from_lang=from_language_key, to=to_language_key)

            # Output to translated text textbox
            self.textEditTo.setText(str(words))

        except Exception as e:
            QMessageBox.about(self, "Translator", str(e))

    def clear(self):
        """Clear the text and reset the languages from/to translate"""
        # Clear the text boxes
        self.textEditFrom.setText('')
        self.textEditTo.setText('')

        # Reset the combo boxes

    def get_language_key(self, name: str) -> Union[str, None]:
        """Get a language key from a name"""
        for key, value in self.languages.items():
            if value == name:
                return key
        return None


if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = Ui()
    sys.exit(app.exec())
