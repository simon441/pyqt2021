from typing import List
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QPushButton
from PyQt5 import uic
from pathlib import Path
import sys

class TicTacToeUi(QMainWindow):
    def __init__(self):
        super(TicTacToeUi, self).__init__()
    
        # Load the ui file
        uic.loadUi(Path.joinpath(Path(__file__).parent.resolve(), "toe.ui"), self)

        # Define a Counter to keep track of who's turn it is
        self.counter = 0

        # Define the widgets
        self.button1 = self.findChild(QPushButton, 'pushButton_1')
        self.button2 = self.findChild(QPushButton, 'pushButton_2')
        self.button3 = self.findChild(QPushButton, 'pushButton_3')
        self.button4 = self.findChild(QPushButton, 'pushButton_4')
        self.button5 = self.findChild(QPushButton, 'pushButton_5')
        self.button6 = self.findChild(QPushButton, 'pushButton_6')
        self.button7 = self.findChild(QPushButton, 'pushButton_7')
        self.button8 = self.findChild(QPushButton, 'pushButton_8')
        self.button9 = self.findChild(QPushButton, 'pushButton_9')
        self.resetButton: QPushButton = self.findChild(QPushButton, 'ResetPushButton')
        self.label: QLabel = self.findChild(QLabel, 'label')


        # Actions
        # Click the Game buttons
        self.button1.clicked.connect(lambda: self.onClicked(self.button1))
        self.button2.clicked.connect(lambda: self.onClicked(self.button2))
        self.button3.clicked.connect(lambda: self.onClicked(self.button3))
        self.button4.clicked.connect(lambda: self.onClicked(self.button4))
        self.button5.clicked.connect(lambda: self.onClicked(self.button5))
        self.button6.clicked.connect(lambda: self.onClicked(self.button6))
        self.button7.clicked.connect(lambda: self.onClicked(self.button7))
        self.button8.clicked.connect(lambda: self.onClicked(self.button8))
        self.button9.clicked.connect(lambda: self.onClicked(self.button9))

        # Click the Start Over (Reset) button
        self.resetButton.clicked.connect(self.reset)

        # Show The App
        self.show()
    
    def onClicked(self, b: QPushButton):
        """ Action when one of the buttons is clicked """
        # show X or O depending on the player's turn
        if self.counter % 2 == 0:
            mark = "X" 
            self.label.setText("O's Turn")
        else:
            mark = "O"
            self.label.setText("X's Turn")

        # Set the button
        b.setText(mark)
        b.setEnabled(False)

        # Increment the counter
        self.counter += 1

    def reset(self):
        """ Start Over """
        # List of all the game buttons
        buttonList: List[QPushButton] = [
            self.button1,
            self.button2,
            self.button3,
            self.button4,
            self.button5,
            self.button6,
            self.button7,
            self.button8,
            self.button9,
        ]

        # Reset text and re-anable each game button
        for b in buttonList:
            b.setText("")
            b.setEnabled(True)
        
        # Reset the label
        self.label.setText("X Goes First")

        # Reset the counter
        self.counter = 0

if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = TicTacToeUi()
    sys.exit(app.exec_())
