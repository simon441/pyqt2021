import PyQt5.QtWidgets as qtw
import PyQt5.QtGui as qtg


class MainWindow(qtw.QWidget):

    def __init__(self):
        super().__init__()

        # set window title
        self.setWindowTitle("Hello World!")

        # Set layout
        self.setLayout(qtw.QVBoxLayout())

        # Create a Label
        my_label = qtw.QLabel("Pick something from the list below")

        # Change the font size of the label
        my_label.setFont(qtg.QFont("Helvetica", 24))
        # add it to the layout
        self.layout().addWidget(my_label)

        # Create a spin box
        # QSpinBox for integers and QDoubleSpinBox for floats
        my_spin = qtw.QSpinBox(self, value=10,
                               maximum=100,
                               minimum=0,
                               singleStep=10,
                               prefix="#"
                               )
        # my_spin = qtw.QDoubleSpinBox(self, value=10,
        #                        maximum=100,
        #                        minimum=0,
        #                        singleStep=.5,
        #                        prefix="#"
        #                        )
        # Change font size of spinbox
        my_spin.setFont(qtg.QFont("Helvetica", 18))
        my_spin.setSuffix(" Order")

        # Put spin box on the screen
        self.layout().addWidget(my_spin)

        # Create a button
        my_button = qtw.QPushButton(
            "Press Me!", clicked=lambda: press_it())
        # add it to the layout
        self.layout().addWidget(my_button)

        # Show the app window
        self.show()

        def press_it():
            # Add name to the label
            my_label.setText(
                f"You Picked {my_spin.value()} {my_spin.valueFromText()}!")


app = qtw.QApplication([])
mw = MainWindow()

# Run the App
app.exec_()
