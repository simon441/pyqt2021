

import sys
from datetime import datetime
from pathlib import Path

from PyQt5 import uic
from PyQt5.QtCore import QTimer
from PyQt5.QtWidgets import QApplication, QLCDNumber, QMainWindow


class Ui(QMainWindow):
    def __init__(self) -> None:
        super(Ui, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'lcd.ui'), self)

        # Define the widgets
        self.lcd: QLCDNumber = self.findChild(QLCDNumber, 'lcdNumber')

        # Create a Timer
        self.timer = QTimer()
        self.timer.timeout.connect(self.timer_tracker)

        # Start the timer and update every second
        self.timer.start(1000)

        # Call the timing tracker function
        self.timer_tracker()

    def timer_tracker(self):
        """Tracker the QTimer"""
        # Get the time
        time = datetime.now()
        formatted_time = time.strftime('%H:%M:%S')

        # Set number of LCD digits to show
        self.lcd.setDigitCount(12)
        # Make text flat (no white outline)
        self.lcd.setSegmentStyle(QLCDNumber.SegmentStyle.Flat)

        # Display the current time
        self.lcd.display(formatted_time)

        # Show the App
        self.show()


if __name__ == '__main__':
    # Initialize the App
    app = QApplication(sys.argv)
    UIWindow = Ui()
    sys.exit(app.exec_())
