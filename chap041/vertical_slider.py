import sys
from pathlib import Path

from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QLabel, QLayout, QMainWindow, QSlider


class Ui(QMainWindow):
    def __init__(self) -> None:
        super().__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'slide.ui'), self)
        self.setWindowTitle('Slider!')

        # Define the Widgets
        slider: QSlider = self.findChild(
            QSlider, name='verticalSlider')
        self.label: QLabel = self.findChild(QLabel, 'label')

        # Set Slider Properties
        slider.setMinimum(0)
        slider.setMaximum(50)
        slider.setValue(0)
        slider.setTickPosition(QSlider.TickPosition.TicksLeft)
        slider.setTickInterval(5)
        slider.setSingleStep(5)
        

        # Move the Slider
        slider.valueChanged.connect(self.slide_it)

        # Show the App
        self.show()

    def slide_it(self, value: int):
        print(value, type(value))
        self.label.setText(str(value))


if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = Ui()
    sys.exit(app.exec_())
