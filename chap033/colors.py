from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic
import sys
from pathlib import Path

class UI(QMainWindow):
    def __init__(self) -> None:
        super(UI, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(Path(__file__).parent.resolve(), 'colors.ui'), self)

        # Add Menu Triggers
        self.actionBlack.triggered.connect(lambda: self.change('black'))
        self.actionWhite.triggered.connect(lambda: self.change('white'))
        self.actionRed.triggered.connect(lambda: self.change('red'))        
        self.actionBlue.triggered.connect(lambda: self.change('blue'))
        self.actionGreen.triggered.connect(lambda: self.change('green'))
        self.actionYellow.triggered.connect(lambda: self.change('yellow'))

        # Set the window title
        self.setWindowTitle('Change Background Color')

        # Show the App
        self.show()

    def change(self, color: str) -> None:
        """ Change the color """
        # Change background color
        self.setStyleSheet(f'background-color: {color};')
        # Change the title
        self.setWindowTitle(f'You Changed The Color To {color.title()}')


if __name__ == '__main__':
    # Initialize the App
    app = QApplication(sys.argv)
    UIWindow = UI()
    sys.exit(app.exec_())
