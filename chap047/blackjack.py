import random
import sys
from pathlib import Path
from typing import List

from PyQt5 import uic
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QPushButton


class Ui(QMainWindow):

    def __init__(self, maxCardsPerPlayer=5) -> None:
        """[summary]

        Args:
            maxCardsPerPlayer (int, optional): [Maximum cards per player]. Defaults to 5.
        """
        super(Ui, self).__init__()

        self.maxCardsPerPlayer = maxCardsPerPlayer

        # Define variables
        # Define the initial Deck
        self.deck: List[str] = []
        # Player's decks
        self.dealer: List[str] = []
        self.player: List[str] = []

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'blackjack.ui'), self)
        self.setWindowTitle('Blackjack')

        # Define widgets
        self.dealerHeaderLabel: QLabel = self.findChild(
            QLabel, 'dealerHeaderLabel')
        self.playerHeaderLabel: QLabel = self.findChild(
            QLabel, 'playerHeaderLabel')

        self.dealerCard1: QLabel = self.findChild(QLabel, 'dealerCardLabel_1')
        self.dealerCard2: QLabel = self.findChild(QLabel, 'dealerCardLabel_2')
        self.dealerCard3: QLabel = self.findChild(QLabel, 'dealerCardLabel_3')
        self.dealerCard4: QLabel = self.findChild(QLabel, 'dealerCardLabel_4')
        self.dealerCard5: QLabel = self.findChild(QLabel, 'dealerCardLabel_5')

        self.playerCard1: QLabel = self.findChild(QLabel, 'playerCardLabel_1')
        self.playerCard2: QLabel = self.findChild(QLabel, 'playerCardLabel_2')
        self.playerCard3: QLabel = self.findChild(QLabel, 'playerCardLabel_3')
        self.playerCard4: QLabel = self.findChild(QLabel, 'playerCardLabel_4')
        self.playerCard5: QLabel = self.findChild(QLabel, 'playerCardLabel_5')

        shuffleButton: QPushButton = self.findChild(
            QPushButton, 'shufflePushButton')
        hitMeButton: QPushButton = self.findChild(
            QPushButton, 'hitPushButton')
        standPushButton: QPushButton = self.findChild(
            QPushButton, 'standPushButton')

        # Click buttons
        shuffleButton.clicked.connect(self.shuffleCards)
        hitMeButton.clicked.connect(self.playerHit)

        # Shuffle the Cards
        self.shuffleCards()

        # Show the App
        self.show()

    def shuffleCards(self):
        """Shuffle the cards
        """
        # Reset card images
        pixmap = QPixmap(str(Path.joinpath(
            Path(__file__).parent.parent.resolve(), 'images', 'green.png')))
        self.dealerCard1.setPixmap(pixmap)
        self.dealerCard2.setPixmap(pixmap)
        self.dealerCard3.setPixmap(pixmap)
        self.dealerCard4.setPixmap(pixmap)
        self.dealerCard5.setPixmap(pixmap)

        self.playerCard1.setPixmap(pixmap)
        self.playerCard2.setPixmap(pixmap)
        self.playerCard3.setPixmap(pixmap)
        self.playerCard4.setPixmap(pixmap)
        self.playerCard5.setPixmap(pixmap)
        # Define the Deck
        suits = ['clubs', 'diamonds', 'hearts', 'spades']
        # 11 => Jack, 12 => Queen, 13 => King, 14 => Ace
        values = range(2, 15)

        # Create a Deck
        self.deck.clear()

        for suit in suits:
            for value in values:
                self.deck.append(f'{value}_of_{suit}')

        # Create the Players
        self.dealer.clear()
        self.player.clear()
        self.dealerSpot = 0
        self.playerSpot = 0

        self.dealerHit()
        self.dealerHit()

        self.playerHit()
        self.playerHit()

    def dealCards(self):
        """Deal the Cards"""
        try:
            # Grab a random card for the dealer
            card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(card)
            # Add that card to dealer's list
            self.dealer.append(card)
            # Ouput current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
            pixmap = QPixmap(f'{path}')
            self.dealerCard1.setPixmap(pixmap)

            # Grab a random card for the player
            card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(card)
            # Add that card to player's list
            self.player.append(card)
            # Ouput current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
            pixmap = QPixmap(f'{path}')
            self.playerCard1.setPixmap(pixmap)

            if (len(self.deck) <= 0):
                raise IndexError()

            # Update Title bar
            self.setWindowTitle(
                f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
        except IndexError as e:
            self.setWindowTitle('Game Over')

    def dealerHit(self):
        """Deal the Cards for the Dealer"""
        if self.dealerSpot < self.maxCardsPerPlayer:
            try:
                # Grab a random card for the dealer
                card = random.choice(self.deck)
                # Remove that card from the Deck
                self.deck.remove(card)
                # Add that card to dealer's list
                self.dealer.append(card)
                # Ouput current card to screen
                path = Path.joinpath(
                    Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
                pixmap = QPixmap(f'{path}')

                if self.dealerSpot == 0:
                    self.dealerCard1.setPixmap(pixmap)

                elif self.dealerSpot == 1:
                    self.dealerCard2.setPixmap(pixmap)

                elif self.dealerSpot == 2:
                    self.dealerCard3.setPixmap(pixmap)

                elif self.dealerSpot == 3:
                    self.dealerCard4.setPixmap(pixmap)

                elif self.dealerSpot == 4:
                    self.dealerCard5.setPixmap(pixmap)

                self.dealerSpot += 1

                if (len(self.deck) <= 0):
                    raise IndexError()

                # Update Title bar
                self.setWindowTitle(
                    f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
            except IndexError as e:
                self.setWindowTitle('Game Over')

    def playerHit(self):
        """Deal the Cards for the Player"""
        if self.playerSpot < self.maxCardsPerPlayer:
            try:
                # Grab a random card for the player
                card = random.choice(self.deck)
                # Remove that card from the Deck
                self.deck.remove(card)
                # Add that card to player's list
                self.player.append(card)
                # Ouput current card to screen
                path = Path.joinpath(
                    Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
                pixmap = QPixmap(f'{path}')

                if self.playerSpot == 0:
                    self.playerCard1.setPixmap(pixmap)

                elif self.playerSpot == 1:
                    self.playerCard2.setPixmap(pixmap)

                elif self.playerSpot == 2:
                    self.playerCard3.setPixmap(pixmap)

                elif self.playerSpot == 3:
                    self.playerCard4.setPixmap(pixmap)

                elif self.playerSpot == 4:
                    self.playerCard5.setPixmap(pixmap)

                self.playerSpot += 1

                if (len(self.deck) <= 0):
                    raise IndexError()

                # Update Title bar
                self.setWindowTitle(
                    f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
            except IndexError as e:
                self.setWindowTitle('Game Over')


# Initialize the Application
if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = Ui()
    sys.exit(app.exec())
