from PyQt5.QtWidgets import QMainWindow, QApplication, QComboBox, QLabel,QPushButton
from  PyQt5 import uic
import sys
from pathlib import Path
from os import sep

class Ui(QMainWindow):

    def __init__(self):
        super(Ui, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(Path(__file__).parent.resolve(), 'dependant_combo.ui'), self)

        # Define the Widgets
        self.combo1 : QComboBox = self.findChild(QComboBox, 'comboBox')
        self.combo2 : QComboBox = self.findChild(QComboBox, 'comboBox2')
        self.label: QLabel = self.findChild(QLabel, 'label')


        # Add items to the first comboBox
        self.combo1.addItem('Female', ['Alice', 'Yasmir', 'Yuki'])
        self.combo1.addItem('Male', ['Farid', 'John', 'Yami'])

        # Action on Click the first dropdown
        self.combo1.activated.connect(self.onCombo1Activated)
        self.combo2.activated.connect(self.onCombo2Activated)

        # Show the App
        self.show()

    def onCombo1Activated(self, index):
        """ Change the first QComboBox """
        # Clear the second box
        self.combo2.clear()
        # Dependant combo: Populate the second combo with the content of the first
        self.combo2.addItems(self.combo1.itemData(index))


    def onCombo2Activated(self, index):
        """ Change the second QComboBox """
        self.label.setText(f'You picked:  {self.combo2.currentText()}')

# Initialize the App
if __name__ == '__main__':
    app = QApplication(sys.argv)
    UIWindow = Ui()
    sys.exit(app.exec_())
