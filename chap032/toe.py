from typing import List
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QPushButton
from PyQt5 import uic
from pathlib import Path
import sys


class TicTacToeUi(QMainWindow):
    def __init__(self):
        super(TicTacToeUi, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), "toe.ui"), self)

        # Define a Counter to keep track of who's turn it is
        self.counter = 0

        # Define the widgets
        self.button1: QPushButton = self.findChild(QPushButton, 'pushButton_1')
        self.button2: QPushButton = self.findChild(QPushButton, 'pushButton_2')
        self.button3: QPushButton = self.findChild(QPushButton, 'pushButton_3')
        self.button4: QPushButton = self.findChild(QPushButton, 'pushButton_4')
        self.button5: QPushButton = self.findChild(QPushButton, 'pushButton_5')
        self.button6: QPushButton = self.findChild(QPushButton, 'pushButton_6')
        self.button7: QPushButton = self.findChild(QPushButton, 'pushButton_7')
        self.button8: QPushButton = self.findChild(QPushButton, 'pushButton_8')
        self.button9: QPushButton = self.findChild(QPushButton, 'pushButton_9')
        self.resetButton: QPushButton = self.findChild(
            QPushButton, 'ResetPushButton')
        self.label: QLabel = self.findChild(QLabel, 'label')

        # Actions
        # Click the Game buttons
        self.button1.clicked.connect(lambda: self.onClicked(self.button1))
        self.button2.clicked.connect(lambda: self.onClicked(self.button2))
        self.button3.clicked.connect(lambda: self.onClicked(self.button3))
        self.button4.clicked.connect(lambda: self.onClicked(self.button4))
        self.button5.clicked.connect(lambda: self.onClicked(self.button5))
        self.button6.clicked.connect(lambda: self.onClicked(self.button6))
        self.button7.clicked.connect(lambda: self.onClicked(self.button7))
        self.button8.clicked.connect(lambda: self.onClicked(self.button8))
        self.button9.clicked.connect(lambda: self.onClicked(self.button9))

        # Click the Start Over (Reset) button
        self.resetButton.clicked.connect(self.reset)

        # Show The App
        self.show()

    def checkWin(self):
        """ Check if win """
        # Across
        if self.button1.text() != '' and self.button1.text() == self.button4.text() and self.button1.text() == self.button7.text():
            self.win(self.button1, self.button4, self.button7)
            return

        if self.button2.text() != '' and self.button2.text() == self.button5.text() and self.button2.text() == self.button8.text():
            self.win(self.button2, self.button5, self.button8)
            return

        if self.button3.text() != '' and self.button3.text() == self.button6.text() and self.button3.text() == self.button9.text():
            self.win(self.button3, self.button6, self.button9)
            return
        
        # Down
        if self.button1.text() != '' and self.button1.text() == self.button2.text() and self.button1.text() == self.button3.text():
            self.win(self.button1, self.button2, self.button3)
            return

        if self.button4.text() != '' and self.button4.text() == self.button5.text() and self.button4.text() == self.button6.text():
            self.win(self.button4, self.button5, self.button6)
            return

        if self.button7.text() != '' and self.button7.text() == self.button8.text() and self.button7.text() == self.button9.text():
            self.win(self.button7, self.button8, self.button9)
            return
        
        # Diagonal
        if self.button1.text() != '' and self.button1.text() == self.button5.text() and self.button1.text() == self.button9.text():
            self.win(self.button1, self.button5, self.button9)
            return

        if self.counter == 9:
            self.label.setText('Draw')

        if self.button3.text() != '' and self.button3.text() == self.button5.text() and self.button3.text() == self.button7.text():
            self.win(self.button3, self.button5, self.button7)
            return


    def win(self, a: QPushButton, b: QPushButton, c: QPushButton):
        """ Show who won """
        # Change the button colors to red
        a.setStyleSheet('QPushButton {color: red;}')
        b.setStyleSheet('QPushButton {color: red;}')
        c.setStyleSheet('QPushButton {color: red;}')
        
        # Add Winner to label
        self.label.setText(f'{a.text()} Wins!')

        # Disable the Board
        self.disable_board()

    def onClicked(self, b: QPushButton):
        """ Action when one of the buttons is clicked """
        # show X or O depending on the player's turn
        if self.counter % 2 == 0:
            mark = "X"
            self.label.setText("O's Turn")
        else:
            mark = "O"
            self.label.setText("X's Turn")

        # Set the button
        b.setText(mark)
        b.setEnabled(False)

        # Increment the counter
        self.counter += 1

        # Check if won
        self.checkWin()

    def reset(self):
        """ Start Over """
        # List of all the game buttons
        buttonList: List[QPushButton] = [
            self.button1,
            self.button2,
            self.button3,
            self.button4,
            self.button5,
            self.button6,
            self.button7,
            self.button8,
            self.button9,
        ]

        # Reset text and re-anable each game button
        for b in buttonList:
            b.setText("")
            b.setEnabled(True)
            b.setStyleSheet('QPushButton {color: #797979;}')

        # Reset the label
        self.label.setText("X Goes First")

        # Reset the counter
        self.counter = 0

    def disable_board(self):
        """ Disable the board """
        buttonList: List[QPushButton] = [
            self.button1,
            self.button2,
            self.button3,
            self.button4,
            self.button5,
            self.button6,
            self.button7,
            self.button8,
            self.button9,
        ]

        # Reset text and re-anable each game button
        for b in buttonList:
            b.setDisabled(True)      

if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = TicTacToeUi()
    sys.exit(app.exec_())
