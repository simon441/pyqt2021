# PyQt5 GUI Thursdays

by Codemy.com
playlist: [https://youtube.com/playlist?list=PLCC34OHNcOtpmCA8s_dpPMvQLyHbvxocY]

1. Install PyQt5 And Build Simple GUI App - PyQt5 GUI Thursdays #1 [https://youtu.be/rZcdhles6vQ] -> Jan 21, 2021
2. How To Create Combo Boxes - PyQt5 GUI Thursdays #2 [https://youtu.be/O58FGYYBV7U]
3. How To Create Spin Boxes - PyQt5 GUI Thursdays #3 [https://youtu.be/2NyculhiSh4]
4. How To Create Text Boxes - PyQt5 GUI Thursdays #4
5. How To Build Forms With QFormLayout - PyQt5 GUI Thursdays #5
6. PyQT5 Designer Drag and Drop GUI - PyQt5 GUI Thursdays #6
7. Install PyQT for Mac -  #7
8. Build a Calculator With PyQT5 Designer - PyQt5 GUI Thursdays #8
9. Build Calculator Functionality - PyQt5 GUI Thursdays #9Adding Toolbars and Menus with PyQt5 Designer - PyQt5 GUI Thursdays #10
10. Adding Toolbars and Menus with PyQt5 Designer - PyQt5 GUI Thursdays #10
11. Tabs With PyQT5 Designer - PyQt5 GUI Thursdays #11
12. How To Destroy Tabs - PyQt5 GUI Thursdays #12
13. ToDo List GUI App - PyQt5 GUI Thursdays #13
14. Add A Database To Our ToDo List part 1 - PyQt5 GUI Thursdays #14
15. Using SQLite3 For Our ToDo List App part 2 - PyQt5 GUI Thursdays #15
16. Using The Command Link Button - PyQt5 GUI Thursdays #16
17. Using The Statusbar - PyQt5 GUI Thursdays #17
18. Radio Buttons - PyQt5 GUI Thursdays #18
19. Radio Buttons Without Push Buttons - PyQt5 GUI Thursdays #19
20. CheckBoxes - PyQt5 GUI Thursdays #20
21. CheckBoxes Without Buttons - PyQt5 GUI Thursdays #21
22. How To Use The Dial Widget - PyQt5 GUI Thursdays #22
23. Designer Combo Boxes - PyQt5 GUI Thursdays #23
24. How To Open A Second Window - PyQt5 GUI Thursdays #24
25. Pass Data Between Windows - PyQt5 GUI Thursdays #25
26. Hide First Window From Second Window - PyQt5 GUI Thursdays #26
27. How To Load PYQT5 Designer UI File - PyQt5 GUI Thursdays #27
28. Dependent Comboboxes - PyQt5 GUI Thursdays #28
29. File Dialog Boxes With QFileDialog - PyQt5 GUI Thursdays #29
30. Build An Image Viewer App - PyQt5 GUI Thursdays #30
31. Build A Tic Tac Toe Game - PyQt5 GUI Thursdays #31
32. Tic Tac Toe Gameplay Logic - PyQt5 GUI Thursdays #32
33. Change Background Color With Menu - PyQt5 GUI Thursdays #33
34. How To Use The Calendar Widget - PyQt5 GUI Thursdays #34
35. Create An LCD Clock - PyQt5 GUI Thursdays #35
36. Multiple Windows Inside Your App - PyQt5 GUI Thursdays #36
37. Hover and Focus Effects For Forms and Buttons - PyQt5 GUI Thursdays #37
38. Language Translation App! - PyQt5 GUI Thursdays #38
39. How To Add Text To Speech - PyQt5 GUI Thursdays #39
40. Horizontal Sliders - PyQt5 GUI Thursdays #40
41. Vertical Sliders - PyQt5 GUI Thursdays #41
42. Add Text To Images With Pillow - PyQt5 GUI Thursdays #41
43. How To Hide or Show Widgets From GUI - PyQt5 GUI Thursdays #42
44. Bind Text Box Text To Label - PyQt5 GUI Thursdays #44
45. Create And Deal A Deck Of Cards - PyQt5 GUI Thursdays #45
46. Create The Card Game "War" - PyQt5 GUI Thursdays #46
47. Build A Blackjack Card Game - PyQt5 GUI Thursdays #47
48. Check For Blackjack - PyQt5 GUI Thursdays #48
49. Push Blackjack Tie - PyQt5 GUI Thursdays #49
50. Blackjack Hit Me - PyQt5 GUI Thursdays #50
51. Blackjack Convert Aces To One Point - PyQt5 GUI Thursdays #51
52. Blackjack Player Stand And Dealer Hit - PyQt5 GUI Thursdays #52
53. Blackjack Five Card Win - PyQt5 GUI Thursdays #53

## Install

Create a virtual environement: `python -m venv ./venv`
Activate it: Windows: `venv\Scripts\Activate.ps1` (powershell) or `venv\Scripts\Activate.bat` (command prompt). Bash: ```source ./venv/Scripts/activate```
Install dependancies: ```pip install -r requirements.txt```

## PQtDesigner

Convert ui file to py file:
```pyuic5 -x [input].ui -o [output].py```
