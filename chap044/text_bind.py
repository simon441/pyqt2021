from pathlib import Path
import sys
from PyQt5.QtWidgets import QApplication, QLabel, QLineEdit, QMainWindow
from PyQt5 import uic


class Ui(QMainWindow):
    def __init__(self):
        super(Ui, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'text_bind.ui'), self)
        self.setWindowTitle('Bind Text Typing')

        # Define the widgets
        self.edit: QLineEdit = self.findChild(QLineEdit, 'lineEdit')
        self.label: QLabel = self.findChild(QLabel, 'label')

        # Hit Enter Button
        self.edit.editingFinished.connect(self.hitEnter)

        # On Click
        self.edit.textChanged.connect(self.onChangeText)

        # Show the App
        self.show()

    def hitEnter(self):
        # On hit enter key in the lineEdit field
        self.label.setText(self.edit.text())

    def onChangeText(self):
        # On click
        self.label.setText(self.edit.text())


if __name__ == '__main__':
    # Initialize the app
    app = QApplication(sys.argv)
    UIWindow = Ui()
    sys.exit(app.exec_())
