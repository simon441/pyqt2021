from typing import Union
from PyQt5.QtCore import QObject
from PyQt5.QtWidgets import QApplication, QMainWindow
from PyQt5 import uic
import sys
from pathlib import Path


class Ui(QMainWindow):
    # Number of windows
    count = 0

    def __init__(self) -> None:
        super(Ui, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'css_ui.ui'), self)
        # Show the App
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    UIWindow = Ui()
    sys.exit(app.exec_())
