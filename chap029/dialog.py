from PyQt5.QtWidgets import QApplication, QFileDialog, QLabel, QMainWindow, QPushButton
from PyQt5 import uic
import sys
from pathlib import Path

class DialogUi(QMainWindow):

    def __init__(self):
        super(DialogUi, self).__init__()

        # Load the UI file
        uic.loadUi(Path.joinpath(Path(__file__).parent.resolve(), 'dialog.ui'), self)

        # Define the Widgets
        self.button = self.findChild(QPushButton, 'openfilePushButton')
        self.label = self.findChild(QLabel, 'label')

        # Action on Dialog Box Button click
        self.button.clicked.connect(self.onButtonClicked)

        # Show the App
        self.show()

    def onButtonClicked(self):
        """Action when the file open button is clicked"""
        # self.label.setText('You clicked the button')
        # Open File Dialog
        fileName, _ = QFileDialog.getOpenFileName(self, "Open File", str(Path.joinpath(Path(__file__).parent.parent.resolve(), 'images')), "All Files (*);; Python (*.py);;JPG Files (*.jpg, *.jpeg);;PNG Files (*.png)")

        # Output file name to screen
        if fileName:
            print(str(fileName))
            self.label.setText(fileName)

# Initialize the App
if __name__ == '__main__':
    app = QApplication(sys.argv)
    UIWindow = DialogUi()
    sys.exit(app.exec_())

