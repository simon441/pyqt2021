import random
import sys
from pathlib import Path
from typing import List, OrderedDict

from PyQt5 import uic
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QMessageBox, QPushButton


class Ui(QMainWindow):

    def __init__(self, maxCardsPerPlayer=5) -> None:
        """[summary]

        Args:
            maxCardsPerPlayer (int, optional): [Maximum cards per player]. Defaults to 5.
        """
        super(Ui, self).__init__()

        self.maxCardsPerPlayer = maxCardsPerPlayer

        # Define variables
        # Define the initial Deck
        self.deck: List[str] = []
        # Player's decks
        self.dealer: List[str] = []
        self.player: List[str] = []
        # Scores
        self.dealerScore: List[int] = []
        self.playerScore: List[int] = []
        # Blackjack status for each player
        self.blackjackStatus = {'dealer': False, 'player': False}

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'blackjack.ui'), self)
        self.setWindowTitle('Blackjack')

        # Define widgets
        self.dealerHeaderLabel: QLabel = self.findChild(
            QLabel, 'dealerHeaderLabel')
        self.playerHeaderLabel: QLabel = self.findChild(
            QLabel, 'playerHeaderLabel')

        self.dealerCard1: QLabel = self.findChild(QLabel, 'dealerCardLabel_1')
        self.dealerCard2: QLabel = self.findChild(QLabel, 'dealerCardLabel_2')
        self.dealerCard3: QLabel = self.findChild(QLabel, 'dealerCardLabel_3')
        self.dealerCard4: QLabel = self.findChild(QLabel, 'dealerCardLabel_4')
        self.dealerCard5: QLabel = self.findChild(QLabel, 'dealerCardLabel_5')

        self.playerCard1: QLabel = self.findChild(QLabel, 'playerCardLabel_1')
        self.playerCard2: QLabel = self.findChild(QLabel, 'playerCardLabel_2')
        self.playerCard3: QLabel = self.findChild(QLabel, 'playerCardLabel_3')
        self.playerCard4: QLabel = self.findChild(QLabel, 'playerCardLabel_4')
        self.playerCard5: QLabel = self.findChild(QLabel, 'playerCardLabel_5')

        self.shufflePushButton: QPushButton = self.findChild(
            QPushButton, 'shufflePushButton')
        self.hitMeButton: QPushButton = self.findChild(
            QPushButton, 'hitPushButton')
        self.standPushButton: QPushButton = self.findChild(
            QPushButton, 'standPushButton')

        # Click buttons
        self.shufflePushButton.clicked.connect(self.shuffleCards)
        self.hitMeButton.clicked.connect(self.playerHit)

        # Shuffle the Cards
        self.shuffleCards()

        # Show the App
        self.show()

    def shuffleCards(self):
        """Shuffle the cards
        """
        # Reset dictionary keeping track of the blackjack status of each player
        self.blackjackStatus = {'dealer': False, 'player': False}

        # Enable buttons
        self.hitMeButton.setEnabled(True)
        self.standPushButton.setEnabled(True)

        # Reset card images
        pixmap = QPixmap(str(Path.joinpath(
            Path(__file__).parent.parent.resolve(), 'images', 'green.png')))
        self.dealerCard1.setPixmap(pixmap)
        self.dealerCard2.setPixmap(pixmap)
        self.dealerCard3.setPixmap(pixmap)
        self.dealerCard4.setPixmap(pixmap)
        self.dealerCard5.setPixmap(pixmap)

        self.playerCard1.setPixmap(pixmap)
        self.playerCard2.setPixmap(pixmap)
        self.playerCard3.setPixmap(pixmap)
        self.playerCard4.setPixmap(pixmap)
        self.playerCard5.setPixmap(pixmap)
        # Define the Deck
        suits = ['clubs', 'diamonds', 'hearts', 'spades']
        # 11 => Jack, 12 => Queen, 13 => King, 14 => Ace
        values = range(2, 15)

        # Create a Deck
        self.deck.clear()

        for suit in suits:
            for value in values:
                self.deck.append(f'{value}_of_{suit}')

        # Create the Players
        self.dealer.clear()
        self.player.clear()
        self.dealerScore.clear()
        self.playerScore.clear()
        self.dealerSpot = 0
        self.playerSpot = 0

        self.dealerHit()
        self.dealerHit()

        self.playerHit()
        self.playerHit()

    def dealCards(self):
        """Deal the Cards"""
        try:
            # Grab a random card for the dealer
            card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(card)
            # Add that card to dealer's list
            self.dealer.append(card)
            # Ouput current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
            pixmap = QPixmap(f'{path}')
            self.dealerCard1.setPixmap(pixmap)

            # Grab a random card for the player
            card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(card)
            # Add that card to player's list
            self.player.append(card)
            # Ouput current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
            pixmap = QPixmap(f'{path}')
            self.playerCard1.setPixmap(pixmap)

            if (len(self.deck) <= 0):
                raise IndexError()

            # Update Title bar
            self.setWindowTitle(
                f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
        except IndexError as e:
            self.setWindowTitle('Game Over')

    def dealerHit(self):
        """Deal the Cards for the Dealer"""
        if self.dealerSpot < self.maxCardsPerPlayer:
            try:
                # Grab a random card for the dealer
                card = random.choice(self.deck)
                # Remove that card from the Deck
                self.deck.remove(card)
                # Add that card to dealer's list
                self.dealer.append(card)

                # Add Card to Dealers' score
                self.dealerScore.append(self._getCardValue(card))
                # Ouput current card to screen
                path = Path.joinpath(
                    Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
                pixmap = QPixmap(f'{path}')

                if self.dealerSpot == 0:
                    self.dealerCard1.setPixmap(pixmap)

                elif self.dealerSpot == 1:
                    self.dealerCard2.setPixmap(pixmap)

                elif self.dealerSpot == 2:
                    self.dealerCard3.setPixmap(pixmap)

                elif self.dealerSpot == 3:
                    self.dealerCard4.setPixmap(pixmap)

                elif self.dealerSpot == 4:
                    self.dealerCard5.setPixmap(pixmap)

                self.dealerSpot += 1

                if (len(self.deck) <= 0):
                    raise IndexError()

                # Update Title bar
                self.setWindowTitle(
                    f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
            except IndexError as e:
                self.setWindowTitle('Game Over')

            # Check for BlackJack
            self.blackjackCheck('dealer')

    def playerHit(self):
        """Deal the Cards for the Player"""
        if self.playerSpot < self.maxCardsPerPlayer:
            try:
                # Grab a random card for the player
                card = random.choice(self.deck)
                # Remove that card from the Deck
                self.deck.remove(card)
                # Add that card to player's list
                self.player.append(card)

                # Add Card to Players' score
                self.playerScore.append(self._getCardValue(card))
                

                # Ouput current card to screen
                path = Path.joinpath(
                    Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
                pixmap = QPixmap(f'{path}')

                if self.playerSpot == 0:
                    self.playerCard1.setPixmap(pixmap)

                elif self.playerSpot == 1:
                    self.playerCard2.setPixmap(pixmap)

                elif self.playerSpot == 2:
                    self.playerCard3.setPixmap(pixmap)

                elif self.playerSpot == 3:
                    self.playerCard4.setPixmap(pixmap)

                elif self.playerSpot == 4:
                    self.playerCard5.setPixmap(pixmap)

                self.playerSpot += 1

                if (len(self.deck) <= 0):
                    raise IndexError()

                # Update Title bar
                self.setWindowTitle(
                    f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
            except IndexError as e:
                self.setWindowTitle('Game Over')

            # Check for BlackJack
            self.blackjackCheck('player')

    def blackjackCheck(self, playerName: str) -> None:
        """Check for Blackjack (score is 21)

        Args:
            playerName (str): [Player name]
        """
        if playerName in ['player', 'dealer']:
            if playerName == 'dealer':
                self._blackJackMessage(self.dealerScore, 'dealer')
            if playerName == 'player':
                self._blackJackMessage(self.playerScore, 'player')               

            # Check for blackjack
            if len(self.playerScore) == 2 and len(self.dealerScore) == 2:
                # Check for tie
                if self.blackjackStatus['dealer'] == True and self.blackjackStatus['player'] == True:
                    self._showBlackjackMessage('Push!!', "It's a Tie!!")
                # Dealer wins
                elif self.blackjackStatus['dealer'] == True:
                    self._showBlackjackMessage(f'Dealer  Wins!', f'Blackjack!\nDealer Wins!')
                # Player wins
                elif self.blackjackStatus['player'] == True:
                    self._showBlackjackMessage(f'Player Wins!', f'Blackjack!\nPlayer Wins!')

    def _getCardValue(self, cardName: str) -> int:
        """Get card score

        Args:
            cardName (str): card

        Returns:
            int: card value
        """
        card = int(cardName.split('_', 1)[0])
        if card == 14:  # Ace
            return 11
        elif card in (11, 12, 13):
            return 10
        return card

    def _blackJackMessage(self, score: List[int], playerName: str) -> None:
        """If blackjack, show win message

        Args:
            score (List[int]): current player scores
            playerName (str)
        """
        
        if len(score) == 2:  # two cards (start of game
            if score[0] + score[1] == 21:
                # Change blackjack status to Won (True)
                self.blackjackStatus[playerName] = True

                # Disable buttons
                self.hitMeButton.setEnabled(False)
                self.standPushButton.setEnabled(False)

    def _showBlackjackMessage(self, title: str, text: str) -> None:
        """Show the blackjack message in a QMessageBox

        Args:
            title (str): message title
            text (str): message body
        """
        message = QMessageBox(self)
        message.setStyleSheet('color: #fff')
        message.setWindowTitle(title)
        message.setText(text)
        message.show()


# Initialize the Application
if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = Ui()
    sys.exit(app.exec())
