from PyQt5 import uic
import sys
from pathlib import Path
from PyQt5.QtGui import QPixmap

from PyQt5.QtWidgets import QApplication, QFileDialog, QLabel, QMainWindow, QPushButton

class UiImageViewer(QMainWindow):
    def __init__(self):
        super(UiImageViewer, self).__init__()

        # Load the file
        uic.loadUi(Path.joinpath(Path(__file__).parent.resolve(), 'image.ui'), self)

        # Define the Widgets
        self.button = self.findChild(QPushButton, 'openFilePushButton')
        self.label = self.findChild(QLabel, 'imageLabel')

        # Button Action
        self.button.clicked.connect(self.onButtonClicked)

        # Show the App
        self.show()

    def onButtonClicked(self):
        """ Button Clicked Action """

        fileName = QFileDialog.getOpenFileName(self, "Open File", str(Path.joinpath(Path(__file__).parent.resolve().parent.resolve(), 'images')), "All Files (*.*);; JPG Files (*.jpg, *.jpeg);; PNG Files (*.png)")

        if fileName:
            # Open the image
            self.pixmap = QPixmap(fileName[0])
            # Add Pic to label
            self.label.setPixmap(self.pixmap)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = UiImageViewer()
    sys.exit(app.exec_())
