import PyQt5.QtWidgets as qtw
import PyQt5.QtGui as qtg


class MainWindow(qtw.QWidget):

    def __init__(self):
        super().__init__()

        # set window title
        self.setWindowTitle("Hello World!")

        # Set layout
        self.setLayout(qtw.QVBoxLayout())

        # Create a Label
        my_label = qtw.QLabel("Type something in the box below")

        # Change the font size of the label
        my_label.setFont(qtg.QFont("Helvetica", 24))
        # add it to the layout
        self.layout().addWidget(my_label)

        # Create a Text box
        my_text = qtw.QTextEdit(self,
                                lineWrapMode=qtw.QTextEdit.FixedColumnWidth,
                                lineWrapColumnOrWidth=50,
                                placeholderText="Hello World!",
                                readOnly=False
                                )
        my_text.setAcceptRichText(True)
        my_text.setHtml('<h1>Big Header Text</h1>')
        # my_text.setFont(qtg.QFont("Helvetica", 18))

        # Put text box on the screen
        self.layout().addWidget(my_text)

        # Create a button
        my_button = qtw.QPushButton(
            "Press Me!", clicked=lambda: press_it())
        # add it to the layout
        self.layout().addWidget(my_button)

        # Show the app window
        self.show()

        def press_it():
            # Add name to the label
            my_label.setText(
                f"You Typed {my_text.toPlainText()}!")
            my_text.setPlainText("You pressed the button")


app = qtw.QApplication([])
mw = MainWindow()

# Run the App
app.exec_()
