from PyQt5.QtWidgets import QMainWindow, QApplication, QLabel, QTextEdit, QPushButton
from  PyQt5 import uic
import sys
from pathlib import Path
from os import sep

class Ui(QMainWindow):
    
    def __init__(self):
        super(Ui, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(Path(__file__).parent.resolve(), 'loadUi.ui'), self)

        # Define the Widgets
        self.label : QLabel = self.findChild(QLabel, 'label')
        self.textEdit: QTextEdit = self.findChild(QTextEdit, 'textEdit')
        self.button: QPushButton = self.findChild(QPushButton, 'pushButtonSubmit')
        self.button2: QPushButton = self.findChild(QPushButton, 'pushButtonClear')

        # Actions
        self.button.clicked.connect(self.submitAction)
        self.button2.clicked.connect(self.clearAction)

        # Show the App
        self.show()

    def submitAction(self):
        """ Click on Submit button """
        self.label.setText(f'Hello There {self.textEdit.toPlainText()}')
        self.textEdit.setPlainText('')

    def clearAction(self):
        """ Click on Clear button """
        self.textEdit.setText('')
        self.label.setText('Enter your Name...')

# Initialize the App
if __name__ == '__main__':
    app = QApplication(sys.argv)
    UIWindow = Ui()
    sys.exit(app.exec_())

    