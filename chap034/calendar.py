from typing import Union
from PyQt5.QtCore import QDate, QObject
from PyQt5.QtWidgets import QApplication, QCalendarWidget, QLabel, QMainWindow
from PyQt5 import uic
import sys
from pathlib import Path


class UI(QMainWindow):

    def __init__(self) -> None:
        super(UI, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'calendar.ui'), self)

        # Define the widgets
        self.calendar: Union[QCalendarWidget, QObject] = self.findChild(
            QCalendarWidget, 'calendar')
        self.label: Union[QLabel, QObject] = self.findChild(QLabel, 'label')

        # Connect the calendar to the grad_date function
        self.calendar.selectionChanged.connect(self.grab_date)

        # Show the App
        self.show()

    def grab_date(self):
        dateSelected: QDate = self.calendar.selectedDate()

        # Put date on label
        self.label.setText(str(dateSelected))
        self.label.setText(str(dateSelected.toPyDate()))
        self.label.setText(str(dateSelected.toString()))


if __name__ == '__main__':
    # Initialize the App
    app = QApplication(sys.argv)
    UIWindow = UI()
    sys.exit(app.exec_())
