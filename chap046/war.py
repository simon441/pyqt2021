import random
import sys
from typing import List
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton
from PyQt5 import uic
from PyQt5.QtGui import QPixmap
from pathlib import Path


class Ui(QMainWindow):
    def __init__(self,) -> None:
        super(Ui, self).__init__()

        # Define variables
        # Define the initial Deck
        self.deck: List[str] = []
        # Player's decks
        self.dealer: List[str] = []
        self.player: List[str] = []

        # Cards
        self.dealer_card = ''
        self.player_card = ''
        # Keep track of scores
        self.dealer_score = 0
        self.player_score = 0

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'war.ui'), self)
        self.setWindowTitle('War Game')

        # Define widgets
        self.dealerHeaderLabel: QLabel = self.findChild(
            QLabel, 'dealerHeaderLabel')
        self.playerHeaderLabel: QLabel = self.findChild(
            QLabel, 'playerHeaderLabel')
        self.dealerLabel: QLabel = self.findChild(QLabel, 'dealerLabel')
        self.playerLabel: QLabel = self.findChild(QLabel, 'playerLabel')
        shuffleButton: QPushButton = self.findChild(
            QPushButton, 'shufflePushButton')
        dealButton: QPushButton = self.findChild(
            QPushButton, 'dealPushButton')

        # Click buttons
        shuffleButton.clicked.connect(self.shuffleCards)
        dealButton.clicked.connect(self.dealCards)

        # Shuffle the Cards
        self.shuffleCards()

        # Show the App
        self.show()

    def shuffleCards(self):
        """Shuffle the cards
        """
        # Define the Deck
        suits = ['clubs', 'diamonds', 'hearts', 'spades']
        # 11 => Jack, 12 => Queen, 13 => King, 14 => Ace
        values = range(2, 15)

        # Create a Deck
        self.deck.clear()

        for suit in suits:
            for value in values:
                self.deck.append(f'{value}_of_{suit}')

        # Create the Players
        self.dealer.clear()
        self.player.clear()

        # Grab a random card for the dealer
        self.dealer_card = random.choice(self.deck)
        # Remove that card from the Deck
        self.deck.remove(self.dealer_card)
        # Add that card to dealer's list
        self.dealer.append(self.dealer_card)
        # Ouput current card to screen
        path = Path.joinpath(
            Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{self.dealer_card}.png')
        pixmap = QPixmap(f'{path}')
        self.dealerLabel.setPixmap(pixmap)

        # Grab a random card for the player
        self.player_card = random.choice(self.deck)
        # Remove that card from the Deck
        self.deck.remove(self.player_card)
        # Add that card to player's list
        self.player.append(self.player_card)
        # Ouput current card to screen
        path = Path.joinpath(
            Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{self.player_card}.png')
        pixmap = QPixmap(f'{path}')
        self.playerLabel.setPixmap(pixmap)

        # Update Title bar
        self.setWindowTitle(
            f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')

    def dealCards(self):
        """Deal the Cards"""
        try:
            # Grab a random card for the dealer
            self.dealer_card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(self.dealer_card)
            # Add that card to dealer's list
            self.dealer.append(self.dealer_card)
            # Ouput current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{self.dealer_card}.png')
            pixmap = QPixmap(f'{path}')
            self.dealerLabel.setPixmap(pixmap)

            # Grab a random card for the player
            self.player_card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(self.player_card)
            # Add that card to player's list
            self.player.append(self.player_card)
            # Ouput current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{self.player_card}.png')
            pixmap = QPixmap(f'{path}')
            self.playerLabel.setPixmap(pixmap)

            if (len(self.deck) <= 0):
                raise IndexError()

            # Update Title bar
            self.setWindowTitle(
                f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')

            # Update score
            self.score()

        except IndexError as e:
            self.setWindowTitle('Game Over')
            # Tie
            if self.dealer_score == self.player_score:
                self.setWindowTitle(
                    f'Game Over || TIE!  || {self.dealer_score} to {self.player_score}')
            # Dealer wins
            elif self.dealer_score > self.player_score:
                self.setWindowTitle(
                    f'Game Over || Dealer Wins!  || {self.dealer_score} to {self.player_score}')
            # Plzyer wins
            else:
                self.setWindowTitle(
                    f'Game Over || Player Wins!  || {self.player_score} to {self.dealer_score}')

    def score(self):
        """Compute Score
        """
        # Strip out the card number
        dealer_card_id = int(self.dealer_card.split('_', 1)[0])
        player_card_id = int(self.player_card.split('_', 1)[0])

        # Compare the Card Numbers
        # Tie
        if dealer_card_id == player_card_id:
            self.dealerHeaderLabel.setText('Tie!')
            self.playerHeaderLabel.setText('Tie!')
        # Dealer wins
        elif dealer_card_id > player_card_id:
            self.dealerHeaderLabel.setText('Dealer Wins!')
            self.playerHeaderLabel.setText('Player Loses')
            # Update score
            self.dealer_score += 1
        # Player wins
        else:
            self.dealerHeaderLabel.setText('Dealer Loses')
            self.playerHeaderLabel.setText('Player Wins!')
            # Update score
            self.player_score += 1

        self.setWindowTitle(
            f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck || Dealer: {self.dealer_score}    Player: {self.player_score}')


# Initialize the Application
if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = Ui()
    sys.exit(app.exec())
