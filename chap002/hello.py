import PyQt5.QtWidgets as qtw
import PyQt5.QtGui as qtg


class MainWindow(qtw.QWidget):

    def __init__(self):
        super().__init__()

        # set window title
        self.setWindowTitle("Hello World!")

        # Set layout
        self.setLayout(qtw.QVBoxLayout())

        # Create a Label
        my_label = qtw.QLabel("Pick something from the list below")

        # Change the font size of the label
        my_label.setFont(qtg.QFont("Helvetica", 24))
        # add it to the layout
        self.layout().addWidget(my_label)

        # Create a Combo box
        my_combo = qtw.QComboBox(self, editable=True, insertPolicy=qtw.QComboBox.InsertAtTop)
        # Add Items To the Combox box
        my_combo.addItem("Pepperoni", "Something")
        my_combo.addItem("Cheese", 2)
        my_combo.addItem("Mushroom", qtw.QWidget)
        my_combo.addItem("Peppers")
        my_combo.addItems(["One", "Two", "Three"])
        my_combo.insertItem(2, "Third thing")
        my_combo.insertItems(4, ["The thing", "The second thing"])
        # Put Combo box on the screen
        self.layout().addWidget(my_combo)

        # Create a button
        my_button = qtw.QPushButton(
            "Press Me!", clicked=lambda: press_it())
        # add it to the layout
        self.layout().addWidget(my_button)

        # Show the app window
        self.show()

        def press_it():
            # Add name to the label
            my_label.setText(f"You Picked {my_combo.currentIndex()} {my_combo.currentData()}!")


app = qtw.QApplication([])
mw = MainWindow()

# Run the App
app.exec_()
