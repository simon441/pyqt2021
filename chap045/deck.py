import random
import sys
from typing import List
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel, QPushButton
from PyQt5 import uic
from PyQt5.QtGui import QPixmap
from pathlib import Path


class Ui(QMainWindow):
    def __init__(self,) -> None:
        super(Ui, self).__init__()

        # Define variables
        # Define the initial Deck
        self.deck: List[str] = []
        # Player's decks
        self.dealer: List[str] = []
        self.player: List[str] = []

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'deck.ui'), self)
        self.setWindowTitle('Deal Cards')

        # Define widgets
        self.dealerHeaderLabel: QLabel = self.findChild(
            QLabel, 'dealerHeaderLabel')
        self.playerHeaderLabel: QLabel = self.findChild(
            QLabel, 'playerHeaderLabel')
        self.dealerLabel: QLabel = self.findChild(QLabel, 'dealerLabel')
        self.playerLabel: QLabel = self.findChild(QLabel, 'playerLabel')
        shuffleButton: QPushButton = self.findChild(
            QPushButton, 'shufflePushButton')
        dealButton: QPushButton = self.findChild(
            QPushButton, 'dealPushButton')

        # Click buttons
        shuffleButton.clicked.connect(self.shuffleCards)
        dealButton.clicked.connect(self.dealCards)

        # Shuffle the Cards
        self.shuffleCards()

        # Show the App
        self.show()

    def shuffleCards(self):
        """Shuffle the cards
        """
        # Define the Deck
        suits = ['clubs', 'diamonds', 'hearts', 'spades']
        # 11 => Jack, 12 => Queen, 13 => King, 14 => Ace
        values = range(2, 15)

        # Create a Deck
        self.deck.clear()

        for suit in suits:
            for value in values:
                self.deck.append(f'{value}_of_{suit}')

        # Create the Players
        self.dealer.clear()
        self.player.clear()

        # Grab a random card for the dealer
        card = random.choice(self.deck)
        # Remove that card from the Deck
        self.deck.remove(card)
        # Add that card to dealer's list
        self.dealer.append(card)
        # Ouput current card to screen
        path = Path.joinpath(
            Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
        pixmap = QPixmap(f'{path}')
        self.dealerLabel.setPixmap(pixmap)

        # Grab a random card for the player
        card = random.choice(self.deck)
        # Remove that card from the Deck
        self.deck.remove(card)
        # Add that card to player's list
        self.player.append(card)
        # Ouput current card to screen
        path = Path.joinpath(
            Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
        pixmap = QPixmap(f'{path}')
        self.playerLabel.setPixmap(pixmap)

        # Update Title bar
        self.setWindowTitle(
            f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')

    def dealCards(self):
        """Deal the Cards"""
        try:
            # Grab a random card for the dealer
            card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(card)
            # Add that card to dealer's list
            self.dealer.append(card)
            # Ouput current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
            pixmap = QPixmap(f'{path}')
            self.dealerLabel.setPixmap(pixmap)

            # Grab a random card for the player
            card = random.choice(self.deck)
            # Remove that card from the Deck
            self.deck.remove(card)
            # Add that card to player's list
            self.player.append(card)
            # Ouput current card to screen
            path = Path.joinpath(
                Path(__file__).parent.parent.resolve(), 'images', 'card_deck', f'{card}.png')
            pixmap = QPixmap(f'{path}')
            self.playerLabel.setPixmap(pixmap)

            if (len(self.deck) <= 0):
                raise IndexError()

            # Update Title bar
            self.setWindowTitle(
                f'{len(self.deck)} Card{"s" if len(self.deck) > 1 else ""} left in deck...')
        except IndexError as e:
            self.setWindowTitle('Game Over')


# Initialize the Application
if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = Ui()
    sys.exit(app.exec())
