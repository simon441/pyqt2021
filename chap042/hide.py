from PyQt5.QtWidgets import QApplication, QLineEdit, QMainWindow, QPushButton
from PyQt5 import uic

import sys
from pathlib import Path


class Ui(QMainWindow):
    def __init__(self):
        super(Ui, self).__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), "hide.ui"), self)
        self.setWindowTitle("Hide and unhide things")

        # Define Widgets
        button: QPushButton = self.findChild(
            QPushButton, 'hidePushButton')
        self.edit: QLineEdit = self.findChild(QLineEdit, 'editLineEdit')

        # Click the button
        button.clicked.connect(self.toggle_visibility)

        # Show the App
        self.show()

        # Keep track of hidden state (yes or no)
        self.hidden = False

    def toggle_visibility(self):
        """Hide or show widgets"""
        if self.hidden:
            self.edit.show()
            self.hidden = False
        else:
            self.edit.hide()
            self.hidden = True


# Initialize the App
if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = Ui()
    sys.exit(app.exec_())
