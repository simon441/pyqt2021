import sys
from pathlib import Path

from PyQt5 import uic
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QApplication, QLabel, QLineEdit, QMainWindow, QPushButton
from PIL import Image, ImageDraw, ImageFont


class Ui(QMainWindow):
    saved_image_path = Path.joinpath(
        Path(__file__).parent.resolve(), 'images', 'aspen_message.png')

    def __init__(self) -> None:
        super().__init__()

        # Load the ui file
        uic.loadUi(Path.joinpath(
            Path(__file__).parent.resolve(), 'texttoimage.ui'), self)
        self.setWindowTitle('Add Text to Images!')

        # Define the Widgets
        self.button: QPushButton = self.findChild(QPushButton, 'pushButton')
        self.label: QLabel = self.findChild(QLabel, 'label')
        self.edit: QLineEdit = self.findChild(QLineEdit, 'lineEdit')

        # Click the Button
        self.button.clicked.connect(self.addText)

        # Show the App
        self.show()

    def addText(self):
        # Grab the text
        myText = self.edit.text()

        # Open the Image
        myImage = Image.open(Path.joinpath(
            Path(__file__).parent.resolve(), 'images', 'aspen.png'))

        # Define the Font
        textFont = ImageFont.truetype("arial.ttf", 46)

        # Edit the Image
        editImage = ImageDraw.Draw(myImage)
        editImage.text((128, 320), text=myText, fill=('white'), font=textFont)

        # Save the Image
        myImage.save(self.saved_image_path)

        # Update App Image
        pixmap = QPixmap(str(self.saved_image_path))
        self.label.setPixmap(pixmap)

        # Clear the text box
        self.edit.setText("")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    UiWindow = Ui()
    sys.exit(app.exec_())
